#include <stdio.h>
#include <stdlib.h>
# define N 99

struct student_struct{
  int UIN;
  char fname[20];
  char lname[20];
  char score;
};

typedef struct student_struct Student;


int average(Student score[N]);

int main()
{
  FILE* database;
  int lineread;
  Student myclass[N];
  int i;


  database = fopen("ece220-full.csv","r");
  if (database==NULL)
    {
      printf("\n Unable to open file!\n");
      return -1;
    }

  i=0;
  lineread = fscanf(database, "%d %s %s",&myclass[i].UIN,myclass[i].lname, myclass[i].fname);
  while (lineread ==3)
    {
      myclass[i].score = 50 + (rand() %50);
      printf("%d. UIN = %d %s %s score = %d\n", i, myclass[i].UIN,myclass[i].fname, myclass[i].lname, myclass[i].score);
      i++;
      lineread = fscanf(database, "%d %s %s",&(myclass+i)->UIN,(myclass+i)->lname, (myclass+i)->fname);
    } 
  printf("\n Class average = %d \n", average(myclass));

	return 0;
}


int average(Student myclass[N])
{
  int i;
  int sum = 0; 
  for(i=0;i<N;i++)
    sum += myclass[i].score;

  return sum/N;
}
