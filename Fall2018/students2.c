#include <stdio.h>


typedef struct student_tag{
  char name[100];
  int UIN;
  float gpa;
} Student;

void getInput(Student arr[]);
void printStudents(Student arr[], char * filename);


int main()
{
  Student ece220[4];
  getInput(ece220);
  printStudents(ece220, "myoutput.txt");
  return 0;

}


void getInput(Student arr[])
{
  int i;
  char temp;
  for (i=0;i<4;i++)
    {
      scanf("\n%s ", arr[i].name);
      scanf(" %d ", &arr[i].UIN);
      scanf(" %f", &arr[i].gpa); 
      printf("name = %s UIN = %d GPA = %f\n", arr[i].name, arr[i].UIN, arr[i].gpa);
    }

  return;
}

void printStudents(Student arr[], char* filename)
{
  FILE * fileptr;
  int i;
  fileptr = fopen(filename, "w");
  if (fileptr == NULL)
    {
      printf("File could not be opened \n");
      return;
    }
  for (i=0;i<4;i++)
    {
      fprintf(fileptr, "\n name = %s UIN = %d GPA = %f \n", arr[i].name, arr[i].UIN, arr[i].gpa);
    }
  return;
}



