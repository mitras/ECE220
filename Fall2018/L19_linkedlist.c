#include <stdio.h>
#include <stdlib.h>

typedef struct studentStruct Record;
struct studentStruct
{
	int UIN;
	float GPA;
	Record *next;
};

Record *find_node(Record *head, int UIN)
{
	Record *current = head;
	while((current != NULL) && (current->UIN <= UIN))
	{
		if(current->UIN == UIN)
		{
			printf("Student Found, UIN: %d.\n", current->UIN);
			return current;
		}
		current = current->next;
	}
	printf("Student Not Found.\n");
	return NULL;
}


void add_node(Record **list, int new_UIN)
{
	Record *current = *list;
	Record *prev = *list;
	Record *temp = (Record *)malloc(sizeof(Record));
	temp->UIN = new_UIN;
	while(current != NULL)
	{
		if(new_UIN < current->UIN) 
		{
			temp->next = current;
			if(current == *list)
			  {	*list = temp;}
			else
			  {prev->next = temp;}
			return;
		}
		if(current->next == NULL)
		{
			current->next = temp;
			temp->next = NULL;
		}
		prev = current;
		current = current ->next;
	}

}


void remove_node(Record **head, int old_UIN)
{
	Record *prev = *head;
	Record *current = prev;

	while(current != NULL)
	{
		if(current->UIN == old_UIN)
			break;
		prev = current;
		current = current->next;
	}

	if(current == NULL)
		return;
	
	if(prev == *head)
	{
		*head = prev->next;
	}
	else if(current->next == NULL)
	{
		prev->next = NULL;
	}
	else
	{
		prev->next = current->next;
	}

	free(current);
}



int main()
{
	Record *head = (Record *)malloc(sizeof(Record));

	head->UIN = 12345;

	int i;
	Record *current = head;
	
	for(i=1;i<5;i++)
	{
		current->next = (Record *)malloc(sizeof(Record));
		current->next->UIN = i*2+12345;
		current = current->next;
	}
	current->next = NULL;

	current = head;
	for(i=0;i<5;i++)
	{
		printf("Node %d: UIN: %d\n", i, current->UIN);
		current = current->next;
	}

	find_node(head, 12349);

	add_node(&head, 12350);
        current = head;
        for(i=0;i<6;i++)
        {
                printf("Node %d: UIN: %d\n", i, current->UIN);
                current = current->next;
        }

	remove_node(&head, 12350);
	current = head;
	for(i=0;i<6 && (current != NULL);i++)
        {
                printf("Node %d: UIN: %d\n", i, current->UIN);
                current = current->next;
        }

	return 0;
}
