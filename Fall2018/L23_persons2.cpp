#include<iostream>
#include<string>

using namespace std;

class Person
{
protected:
  int id;
  string name;
  string address;
public:
  Person(int id, string name, string address);
  ~Person(){};
  virtual void displayProfile();
  void changeAddress();
};

Person::Person(int id, string name, string address)
{
  this->id = id;
  this->name = name;
  this->address = address;
  return;
}

void Person::displayProfile()
{
  cout << "\n ID: " << this->id << "\n";
  cout << " Name: " << this->name << "\n";
  cout << " Address: " << this->address << "\n";
  return;
}

class Student: public Person
{
protected:
  int course;
  int year;
public:
  Student(int id, string name, string address, int course, int year);
  void displayProfile();
  void updateYear(int newYear){this-> year = newYear;};
};

Student::Student(int id, string name, string address, int year, int course):Person(id, name, address)
{
  this->year = year;
  this->course = course;
  return;
}

void Student::displayProfile()
{
  cout << "\n ID: " << this->id << "\n";
  cout << " Name: " << this->name << "\n";
  cout << " Address: " << this->address << "\n";
  cout << " Year: " << this->year << "\n";
  cout << " Course: " << this->course << "\n";
  return;
}


int main()
{
  Person A (323423,"Sayan Mitra", "Somewhere");
  A.displayProfile();
  Person *B = new Person(42345453, "Malory", "Everywhere");
  B->displayProfile();
  Person* C = new Student(874204, "Tom", "River", 12, 2);
  C->displayProfile();
}
