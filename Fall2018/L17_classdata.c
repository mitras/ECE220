#include<stdio.h>
#include<stdlib.h>
#define N 99

struct StudentStruct{
  int UIN;
  char fname[20];
  char lname[20];
  int score;
};

typedef struct StudentStruct Student;

int average(Student myclass[N]);

int main()
{
  Student myclass[N];
  FILE * fp;
  int readnum;
  int i;

  fp = fopen("ece220-full.csv", "r");
  if (fp == NULL)
    {
      printf("\n File not found");
      return -1;
    }
  i= 0;
  readnum = fscanf(fp,"%d %s %s", &myclass[i].UIN, myclass[i].lname, myclass[i].fname);
  while(readnum==3)
    {
      myclass[i].score = rand() % 100;
      printf("%d. %d %s %s    score = %d \n", i, myclass[i].UIN, myclass[i].fname, myclass[i].lname, myclass[i].score);
      i++;
      readnum = fscanf(fp,"%d %s %s", &myclass[i].UIN, (myclass+i)->lname, (myclass+i)->fname); 
    }


  printf("\n Class avg:  %d \n", average(myclass));

  return 0;
}


int average(Student myclass[N])
{
  int i, sum;
  sum = 0;
  for (i = 0; i< N; i++)
    sum+= myclass[i].score;
  return sum/N;

}
