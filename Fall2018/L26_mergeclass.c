#include "stdio.h"

void mergeSort(int arr[], int l, int r);

void merge(int arr[], int l, int m, int r);

void print(int arr[], int size);

int main()
{
    int arr[] = {15, 3, 5, 2, 1, 9, 18};
    printf("\n Unsorted Array");
    print(arr,7);
    mergeSort(arr,0,6);
    printf("\n Sorted Array");
    print(arr,7);
    return 0;    
}

void print(int arr[], int size)
{
    int i;
    printf("\n");
    for(i=0;i<size;i++)
        printf(" %d", arr[i]);
    return;
}

void mergeSort(int arr[], int l, int r)
{
    int m;
    if (l < r)
    {
        m = l + (r-l)/2 ; 
        mergeSort(arr, l,m);
        mergeSort(arr, m+1,r);
        merge(arr, l,m,r); // merge arr[l,..,m] and arr[m+1,..,r]
    }
    return;
}

void merge(int arr[], int l, int m, int r)
{
    int n1, n2;
    n2 = r-m; 
    n1 = m - l +1;
    int L[n1], R[n2];
    int i, j, k;
    k = l;
    for(i=0;i<n1;i++)
        L[i] = arr[l+i];
    for(j=0;j<n2;j++)
        R[j] = arr[m+1+j];
    
    i = 0; j = 0;
    while(i < n1 && j < n2)
    {
        if (L[i] < R[j])
            {arr[k] = L[i];
            i++; k++;
            }
        else{
            arr[k] = R[j];
            j++; k++;
        }
    }

    while(i < n1)
    {
        arr[k] = L[i]; i++; k++;
    }
    
    while(j < n2)
    {
        arr[k] = R[j]; j++; k++;
    }
}

