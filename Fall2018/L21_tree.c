#include <stdio.h>
#include <stdlib.h>

typedef struct treenode Node;

struct treenode{
    int ID;
    Node* left;    
    Node* right;
};

Node * newnode(int newData);
void insert( Node ** root, int newData);
void inorder(Node * root);

int main()
{
    Node* root = NULL;
    insert(&root,56);
    insert(&root,6);
    insert(&root,64);
    insert(&root,45);
    insert(&root,30);

    printf(" \n Inorder traversal: ");
    inorder(root);
    printf(" \n");

    return 0;
}

Node * newnode(int newData)
{
    Node * new = (Node*)malloc(sizeof(Node));
    new->ID = newData;
    new->left = NULL;
    new->right = NULL;
    return new;
}


void insert( Node ** root, int newData)
{
    if (*node == NULL)
    {
        *node = newnode(newData);
        return;
    }
    else
    {
        if((*node)->ID > newData)
            insert(&(*node)->left,newData);
        else
            insert(&(*node)->right,newData);
    }
}





Node * newnode(int newData)
{
    Node * new = (Node *)malloc(sizeof(Node));
    if (new == NULL)
    {
        printf("\n Failed to create node\n");
        return new;
    }
    else
    {
        new->ID = newData;
        new->left = NULL;
        new->right = NULL;
        return new;
    }
}

void insert( Node ** node, int newData)
{
    if (*node == NULL)
    {
        *node = newnode(newData);
        return;
    }
    else
    {
        if ((*node)->ID > newData)
            insert(&(*node)->left,newData);
        else
            insert(&(*node)->right,newData);
    }
}

void inorder(Node * root)
{
    if (root==NULL)
        return;
    else
        {
            inorder(root->left);
            printf(" %d", root->ID);
            inorder(root->right);
        }
}



