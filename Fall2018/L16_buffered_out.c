
 // Copyright (c) ECE 220 Course Staff 2018 
#include <stdio.h>

int main()
{
  FILE* out_stream;
  FILE* in_stream;
  char str[32];
  
  in_stream = fopen("L16_buffered.c","r");
  if (in_stream==NULL)
    {
      printf("\n file %s not found \n", "L16_buffered.c");
      return -1;
    }
  out_stream = fopen("L16_buffered_out.c","w");
  if (out_stream==NULL)
  {
     printf("\n could not open file %s \n", "L16_buffered_out.c");
     fclose(in_stream);
     return -1;
  }
  fprintf(out_stream,"%s","\n // Copyright (c) ECE 220 Course Staff 2018 \n");

  fgets(str, 32, in_stream);
  while(!feof(in_stream))
    {
      fputs(str, out_stream);
      fgets(str, 32, in_stream);
    }
    
    fclose(in_stream);
    fclose(out_stream);
  return 0;
}
