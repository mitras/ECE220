// Copyright (c) 2018 ECE 220 Course Staff 
 // Program start 
#include <stdio.h>

int main()
{
  FILE* in_file;
  FILE* out_file;

  char str[32];

  in_file = fopen("L16_basicfile.c","r");
  if (in_file == NULL)
    {
      printf("\n Unable to open file %s \n", "L16_basicfile.c");
      return -1;
    }

  out_file = fopen("L16_basicfile_out.c","w");
  if (out_file == NULL)
    {
      printf("\n Unable to open file %s \n", "L16_basicfile_out.c");
      fclose(in_file);
      return -1;
    }
  
  fprintf(out_file,"// Copyright (c) 2018 ECE 220 Course Staff \n // Program start \n");
  fgets(str, 32, in_file);
  while(!feof(in_file))
    {
      fputs(str,out_file);
      fgets(str, 32,in_file);
    }

  fprintf(out_file,"// Copyright (c) 2018 ECE 220 Course Staff \n");

  fclose(in_file);
  fclose(out_file);

}
// Copyright (c) 2018 ECE 220 Course Staff 
