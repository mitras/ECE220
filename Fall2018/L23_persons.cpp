//
//  persons.cpp
//  
//
//  Created by Mitra, Sayan on 11/16/16.
//
//

#include <iostream>
#include <string>

using namespace std;

class Person
{
    protected:
        int id;
        string name;
        string address;
   
    public:
        Person(int id, string name, string address);
        ~Person();
        virtual void displayProfile();
};

class Student : public Person
{
    protected:
        int course;
        int year; // 1 - fr, 2 - s
        //vector<int> classesTaken;
    
    public:
        Student(int id, string name, string address, int course, int year);
        ~Student();
        void displayProfile();
        void updateYear(int y){year = y;};
    
};

Student::Student(int id, string name, string address, int course, int year): Person(id,name,address)
{
    cout << "Entering constructor of Student " << name << endl;
    
    this->course = course;
    this->year = year;
    
    
}

Student::~Student()
{
    cout << "Entering desstructor of Student " << name << endl;
}

Person::Person(int id, string name, string address)
{
    cout << "Entering constructor of person " << name << endl;
    
    this->id = id;
    this-> name = name;
    this->address = address;
    
}


Person::~Person()
{
    cout << "Entering destructor for person " << name << endl;
}

void Person::displayProfile()
{
    cout << "----------------------------" << endl;
    cout << " ID: " << id;
    cout << " Name: " << name;
    cout << " Address: " << address << endl;
    cout << "----------------------------" << endl;
    return;
}

void Student::displayProfile()
{
    cout << "----------------------------" << endl;
    cout << " ID: " << id;
    cout << " Name: " << name;
    cout << " Address: " << address << endl;
    cout << " Courses: " << course;
    cout << " Year: " << year << endl;
    cout << "----------------------------" << endl;
    return;
}

int main()
{
    /*
    Person *john = new Person(12345, "john Doe", "500 E. Univ. Ave.");;
    Person p(653,"sayan", "urbana,il");
    p.displayProfile();
    john->displayProfile();
    Student *tom = new Student(12346, "Tom Sawyer", "Univ.",220,2);
    tom->displayProfile();
    //delete john;
    //delete tom;
    return 0;
     */
     Person *john = new Person(12345, "john Doe", "500 E. Univ. Ave.");;
     Person *steve = new Student(12346, "Tom Sawyer", "Univ.",220,2);
     steve->displayProfile();
    dynamic_cast<Student *>(steve)->updateYear(4);
    steve->displayProfile();

    delete steve;
    
    
}
