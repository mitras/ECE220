#include <stdio.h>

void movetower(int size, int froma, int tob);

int main()
{
  int n;
  scanf("%d", &n);
  printf("Move tower %d from 0 to 2\n", n);
  movetower(n,0,2);
  return(0);
}

void movetower(int n, int a, int b)
// a, b = {0,1,2}, 3 - a - b 
// a = 1 b = 0; 3 - a - b = 2
// a = 1 b = 2; 3 - a - b = 0
// a = 0 b = 2; 3 - a - b = 1
{
  if (n == 1)
    {
      printf("Move disc of size %d from pole %d to pole %d \n", n, a, b);
      return;
    }
  else
    {
      movetower(n-1, a, 3-a-b);
      printf("Move disc of size %d from pole %d to pole %d \n", n, a, b);
      movetower(n-1, 3-a-b, b);
    }
}
      

  
