#include <stdio.h>
#include <stdlib.h>

typedef struct node Node;

struct node{
    int data;
    Node* next;
};

Node * top;

void push(int newData);
int pop();
void print_stack(Node * curr);

int main()
{
    top = NULL;
    push(6);
    push(8);
    push(4);
    pop();
    print_stack(top);
    return 0;
}

void push(int newData){
    Node * new = (Node*)malloc(sizeof(Node));
    new->data = newData;
    new->next = top;
    top = new;
}

int pop(){
    int val;
    Node * temp;
    if(top==NULL)
        return -1;
    else{
        val = top->data;
        temp = top;
        top = top->next;
        free(temp);
        return val;
    }
}

void print_stack(Node * curr)
{
    if (curr==NULL)
        return;
    else
    {
        printf(" %d ", curr->data);
        print_stack(curr->next);
    }
}


