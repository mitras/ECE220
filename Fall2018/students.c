#include <stdio.h>
#include <string.h>

struct student_struct{
  int UIN;
  char fname[20];
  char lname[20];
  char grad;
};

typedef struct student_struct Student;

void printClass(Student* cls, int size);

float avgClass(Student* cls, int size);

int main()
{
  FILE * inputfile;
  int i = 0;
  int numread;
  Student ece220[100];
  inputfile = fopen("ece220-full.csv","r"); 
  do
    {
      numread = fscanf(inputfile, "%d %s %s", &ece220[i].UIN, ece220[i].lname, ece220[i].fname);
      if (numread ==3)
	{
	  //printf("%d. %d %s %s \n",i, ece220[i].UIN, ece220[i].fname, ece220[i].lname);
	  i++;
	}
    }while(numread==3);

  printClass(ece220,99);
  printf(" \n Class average = %f", avgClass(ece220,99));
}


void printClass(Student* cls, int size)
{
  int i;
  for(i=0;i<size;i++)
    printf("%d. %d %s %s \n",i, (cls + i)->UIN, (cls + i)->fname, (cls + i)->lname);
  return;
}

float avgClass(Student* cls, int size)
{
  int i;
  float sum=0;
  for(i=0;i<size;i++)
    sum += (cls+i)->UIN %100;

  return sum/size;
}






