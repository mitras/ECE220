#include<iostream>
using namespace std;
 
class ComplexNumber {
private:
    int real, imag;
public:
    //default constructor
    ComplexNumber()  {real = 0;   imag = 0;}

    //parameterized constructor
    ComplexNumber(int r, int i)  {real = r;   imag = i;}

    //copy constructor
    ComplexNumber(const ComplexNumber &object)  {real = object.real; imag = object.imag;}

    //operator overloading
    ComplexNumber operator + (const ComplexNumber &object) {
         ComplexNumber result;
         result.real = real + object.real;
         result.imag = imag + object.imag;
         return result;
    }
    void print() { cout << real << " + j" << imag << endl; }
};
 
int main()
{
    ComplexNumber c1(1, 2), c2(3, 4);
    ComplexNumber c3 = c1 + c2;
    ComplexNumber c4 = c3;
    c3.print();
    c4.print();
}

