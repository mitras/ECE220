#include <stdio.h>
#include <math.h>
// finding roots of a quadratic equations
/* Sept 2018
   ECE 220
 */

int main()
{
  float a,b,c,det;
  float x1,x2;
  printf("\n Input the coefficients a,b,c:");
  scanf("%f %f %f", &a,&b,&c);
  det = b*b - (4 * a * c);
  if (det == 0)
    {
      x1 = -(b/(2*a));
      x2 = x1;
      printf("\n Repeated roots at %f %f\n", x1, x2);
    }
  else if (det > 0)
    {
      x1 = (-b + sqrt(det))/(2*a);
      x2 = (-b - sqrt(det))/(2*a);
     printf("\n Distinct real roots at %f %f\n", x1, x2);
    }
  else // det < 0
    {
      x1 = -b/(2*a);
      x2 = sqrt(-det)/(2*a);
      printf("\n Distinct complex roots at %f+i%f, and %f-i%f\n", x1,x2, x1, x2);    
    }
  return 0;
}
