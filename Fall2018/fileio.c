#include <stdio.h>
#include <string.h>

struct studentStruct{
  char fname[40];
  char lname[40];
  char grade;
  int UIN;
};

typedef struct studentStruct Student;

void StudentPrint(Student x);

int main()
{
  FILE* myfile;
  FILE* outfile;
  char c;
  myfile = fopen("ece220-full.csv","r");
  outfile = fopen("output.dat", "w");
  Student x,y;
  Student ececlass[100];
  int i = 1;
  int N;
  Student * sptr;
 
  fscanf(myfile,"%d %s %s",&ececlass[0].UIN,ececlass[0].fname,ececlass[0].lname);

 while(!feof(myfile))
   {
     //     if (feof(myfile))
     // break;
     fscanf(myfile,"%d %s %s",&ececlass[i].UIN,ececlass[i].fname,ececlass[i].lname);
     StudentPrint(*(ececlass+i));
     printf(" %d ",i);
     i++;
   }

 N = i-1;
 for(i=0;i<N;i++)
   {
     sptr = &ececlass[i]; 
     sptr->grade = '_';
     StudentPrint(ececlass[i]);
   }
  return 0;
}

void StudentPrint(Student x)
{
  printf("\n Name: %s %s UIN: %d grade: %c\n",x.lname,x.fname,x.UIN,x.grade);
   return;

}
