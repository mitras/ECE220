#include <iostream>
#include <vector>

using namespace std;

template <class T>
class SortedArray{
    private:
        vector<T> arr;
        int size;
        void merge(int l, int m, int r);

    public:
        SortedArray(){size = 0;};
        SortedArray(vector<T> Init)
            {
                int i;
                arr.resize(Init.size());
                for(i =0; i< Init.size(); i++)
                {
                    arr[i] = Init[i];
                }
                size = Init.size();
            };
        void print();
        int getsize(){return arr.size();};
        void mergeSort(int l, int r);
};

template <class T>
void SortedArray<T>::print()
{
    int i;
    cout << "\n";
    for(i=0;i<size;i++)
        cout << " " << arr[i];
    cout << "\n";
    return;
}

template <class T>
void SortedArray<T>::mergeSort(int l, int r)
{
    int m;
    if (l < r)
    {
        m = l + (r-l)/2 ; 
        mergeSort(l,m);
        mergeSort(m+1,r);
        merge(l,m,r); // merge arr[l,..,m] and arr[m+1,..,r]
    }
    return;
}

template <class T>
void SortedArray<T>::merge(int l, int m, int r)
{
    int n1, n2;
    n2 = r-m; 
    n1 = m - l +1;
    vector<T> L; // [n1];
    vector<T> R; // [n2];
    L.resize(n1);
    R.resize(n2);   
    int i, j, k;
    k = l;
    for(i=0;i<n1;i++)
        L[i] = arr[l+i];
    for(j=0;j<n2;j++)
        R[j] = arr[m+1+j];
    
    i = 0; j = 0;
    while(i < n1 && j < n2)
    {
        if (L[i] < R[j])
            {arr[k] = L[i];
            i++; k++;
            }
        else{
            arr[k] = R[j];
            j++; k++;
        }
    }

    while(i < n1)
    {
        arr[k] = L[i]; i++; k++;
    }
    
    while(j < n2)
    {
        arr[k] = R[j]; j++; k++;
    }
}



int main()
{
    vector<char> Init;
    Init.push_back('a');
    Init.push_back('c');
    Init.push_back('e');
    Init.push_back('f');
    Init.push_back('i');
    Init.push_back('x');
    Init.push_back('c');
    Init.push_back('h');
 
    SortedArray<char> MyArray(Init);
    cout << "\n New size of arr=" << MyArray.getsize() << "\n";
    cout << "\n  Unsorted Array" << endl;
    MyArray.print();
    MyArray.mergeSort(0,MyArray.getsize()-1);
    cout << "\n  Sorted Array" << endl;
    MyArray.print();
    return 0;    
}

