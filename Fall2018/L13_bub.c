#include <stdio.h>
#define SIZE 5

int main()
{
	int n = SIZE-1;
	int array[] = {7,4,6,3,1};

	int i, j,k, temp, swap = 0;

	//sort number in ascending order
	
	for(i=0;i<SIZE-1;i++)
	  {
	    for (j=0; j < SIZE-1; j++)
	      {
		if(array[j]>array[j+1])
		  // swap
		  {
		    temp=array[j+1];
		    array[j+1] = array[j];  
		    array[j] = temp;
		  }
	      }
	    printf("sorted array: \n");
	    for(k=0;k<SIZE;k++){
		printf("%d ", array[k]);
	    }
	    printf("\n");
	  }

	printf("sorted array: \n");
	for(i=0;i<SIZE;i++){
		printf("%d ", array[i]);
	}
	printf("\n");

	return 0;
}
