//
//  pointing.c
//  
//
//  Created by Mitra, Sayan on 3/2/15.
//
//

#include <stdio.h>
#define N 3
void printmatrix(int A[N][N], int n);
void initmatrix(int A[N][N], int n);


int main()
{
  int A[N][N] = {1,2,3,4,5,6,7,8,9};
  int B[N][N] = {1000,0,0,100,0,0,10,0,0};
  int C[N][N];
  
  initmatrix(C,N);

  int i,j,k;

  printf(" \n A = \n");
  printmatrix(A,N);
  printf(" \n B = \n");
  printmatrix(B,N);

  // mult
  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      for (k=0;k<N;k++)
	C[i][j] = C[i][j] + A[i][k] * B[k][j];
  
  printf(" \n C = \n");
  printmatrix(C,N);
  matrixmult(A,B,C);
  printmatrix(C,N);
  
  return 0;

}

void matrixmult(int *A, int* B, int* C)
{
  int i, j;
  i = 0; j = 0;
  for(i=0;i<N;i++)
    for(j=0;j<N;j++)
      *(C+N*i + j)=i;
  return;
}

void initmatrix(int A[N][N], int n)
{
  int i, j;
  for(i=0;i<N; i++)
    for(j=0;j<N;j++)
      //      *(A + i*N + j) = 0
      A[i][j] = 0;
  return;
}

void printmatrix(int A[N][N], int n)
{
  int i, j;
  for(i=0;i<N; i++)
    {
    for(j=0;j<N;j++)
      printf(" %4d", A[i][j]);  
    printf("\n");
    }
}
