#include<iostream>
#include<string> 

using namespace std;

class square{
private: 
  int side;
  friend class rectangle;
public:
  square(int s){side = s;};
};

class rectangle
{
private:
	int width, height;
	public: 
	  rectangle(){};
      rectangle(int x, int y){width = x; height = y;};
      void convert(square a){width = height=a.side;}; 
      int area(){return width * height;};
      friend rectangle duplicate(const rectangle& source);
};

rectangle duplicate(const rectangle& source)
{
  rectangle rec;
  rec.height = source.height;
  rec.width = source.width;
  return rec;
}

int main()
{
  rectangle foo;
  rectangle bar(2,3);
  foo = duplicate(bar);
  cout << bar.area() << endl;
  cout << foo.area() << endl;
  square sqr(4);
  foo.convert(sqr);
  cout << foo.area() << endl;
  return 0;
}
      
