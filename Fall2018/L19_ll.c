#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct studentStruct Student;

struct studentStruct
{
  int UIN;
  char lname[20];
  char fname[20];
  Student *next;
};


typedef struct studentStruct Student;


void print(Student * head);
//void find(Student ** head, int tofindUIN);
void add(Student ** head, Student st);
void add_inorder(Student ** head, Student st);
//void remove(Student ** head, int toRemove);


int main()
{
  Student * head;
  Student bob;

  // creating head node
  head = (Student *)malloc(sizeof(Student));
  /*
  if(head == NULL)
    {
      printf("\n Could not create head node");
    }
  head->UIN = 1234;
  strcpy(head->fname, "Alice");
  head->next =  (Student *)malloc(sizeof(Student));
  head->next->UIN = 9134;
  strcpy(head->next->fname, "Raja");
  */

  //  print(head);
  bob.UIN = 6678;
  strcpy(bob.fname,"Bob");
  add(&head, bob);
  print(head);

  return 0;
}

void print(Student * head)
{
  Student * current = head;
  int i=0;
  while (current != NULL)
    {
      printf("\n ------------ \n Node %d \n UIN = %d \n Name = %s %s \n", i, current->UIN, current->fname, current->lname);
      i++;
      current = current->next;
    }
}


void add(Student ** head, Student st)
{
  Student * current = *head;
  Student * prev = *head;

  Student * thisStudent = (Student *)malloc(sizeof(Student));
  thisStudent->UIN = st.UIN;
  strcpy(thisStudent->fname, st.fname);
  strcpy(thisStudent->lname, st.lname);
  thisStudent->next = NULL;
  while (current != NULL)
    {
      prev = current;
      current = current->next;

    }
  prev->next = thisStudent;
  return;

}

void add_inorder(Student ** head, Student st)
{
  Student * current = *head;
  Student * prev = *head;

  Student * thisStudent = (Student *)malloc(sizeof(Student));
  thisStudent->UIN = st.UIN;
  strcpy(thisStudent->fname, st.fname);
  strcpy(thisStudent->lname, st.lname);
  thisStudent->next = NULL;
  while (current != NULL)
    {
      prev = current;
      current = current->next;

    }
  prev->next = thisStudent;
  return;

}

