
#include <stdio.h>
#define N 3
#define M 3
#define P 3

void printmatrix(float A[N][N], int n, int m);
void initmatrix(float A[N][N], int n, int m);
void multiply(float A[N][M], float B[M][P], float C[N][P], int n, int m, int p);

int main()
{
  float A[N][M] = {3,4,5,1,0,0,6,0,1};
  float B[M][P] = {{1,0,0},{0,1,0},{0,0,1}};
  float C[N][P] = {{0,0,0},{0,0,0},{0,0,0}};
  printf("\n A = \n"); 
  printmatrix(A,N,M);
  printf("\n B = \n"); 
  printmatrix(B,M,P);
  printf("\n Before C = \n"); 
  printmatrix(C,N,P);
  multiply(A,B,C,N,M,P);
  printf("\n After C = \n"); 
  printmatrix(C,N,P);
 
  return 0;
}

void printmatrix(float A[N][N], int n, int m)
{
  int i, j;
  for(i=0;i<n;i++)
    {
    for(j=0;j<m;j++) 
      printf(" %0.3f ", A[i][j]);
    printf("\n");
    }  
return;
}


void multiply(float A[N][M], float B[M][P], float C[N][P], int n, int m, int p)
{
  int i,j,k;
  for (i=0;i<n; i++)
    for (j=0;j<p; j++)
      for(k=0;k<m; k++)
	C[i][j] += A[i][k] * B[k][j];
return;
} 





