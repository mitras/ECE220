#include<iostream>
#include<string>

using namespace std;

class Person{
protected:
  int id;
  string name;
  string address;
public:
  Person(int id, string name, string address);
  ~Person(){};
  virtual void displayProfile();
  void changeAdd(string address);
};


  Person::Person(int id, string name, string address)
  {
    this->id = id;
    this->name = name;
    this-> address = address;
    return;
  }

void Person::displayProfile()
{
  cout << "\n Name:" << this-> name <<"\n";
  cout << " Address:" << this-> address <<"\n";
  cout << " ID:" << this->id <<"\n";

}

class Student: public Person
{
protected:
  int courses;
  int year; // 1 = freshmen 
public:
  Student(int id, string name, string address, int courses, int year);
   void displayProfile();
  void updateYear(int newYear);
};

Student::Student(int id, string name, string address, int courses, int year): Person(id, name, address)
{
  this-> courses = courses;
  this->year = year;
}

void Student::displayProfile()
{
  cout << "\n Name:" << this-> name <<"\n";
  cout << " Address:" << this-> address <<"\n";
  cout << " ID:" << this->id <<"\n";
  cout << " Year:" << this->year <<"\n";
  cout << " Courses:" << this->courses <<"\n";
}

void Student::updateYear(int newYear)
{
  this->year = newYear;
}

int main()
{
  Person * B = new Person(873987, "Harry", "13 penn av.");
  B->displayProfile();
  Person*  C = new Student(87987, "Maya", "45 brookine", 4, 2);
  Student * Ccopy = dynamic_cast<Student *>(C);
  Ccopy->displayProfile();
  Ccopy->updateYear(4);
  Ccopy->displayProfile();
 
}



