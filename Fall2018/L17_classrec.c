#include <stdio.h>
#include <stdlib.h>
# define N 99

int average(int score[N]);

int main()
{
  FILE* database;
  int lineread;
  int UIN[N];
  char fname[N][20];
  char lname[N][20];
  int score[N];
  int i;


  database = fopen("ece220-full.csv","r");
  if (database==NULL)
    {
      printf("\n Unable to open file!\n");
      return -1;
    }

  i=0;
  lineread = fscanf(database, "%d %s %s",&UIN[0],lname[0], fname[0]);
  while (lineread ==3)
    {
      score[i] = 50 + (rand() %50);
      printf("%d. UIN = %d %s %s score = %d\n", i, UIN[i],fname[i], lname[i], score[i] );
      i++;
      lineread = fscanf(database, "%d %s %s",&UIN[i],lname[i], fname[i]);
    } 
  printf("\n Class average = %d \n", average(score));

	return 0;
}


int average(int score[N])
{
  int i;
  int sum = 0; 
  for(i=0;i<N;i++)
    sum += score[i];

  return sum/N;
}
