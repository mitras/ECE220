#include<iostream>
#include <cmath>

//using namespace std;

class vector{
    protected: 
        double angle, length; // by default these are private
    public: 
        // constructors
        vector(){ std::cout << "Default constructor called\n"; angle=0.0;length=1.0;};    // default constructor
        vector(double a, double l){ std::cout << "Other constructor called\n"; angle=a;length=l;};
        vector( const vector &obj);  // copy constructor
        //destructor 
        ~vector() { /*std::cout << "\n vector object destructed";*/};
        
        void PrintVector(){std::cout << "\n length:" << length << " angle:" << angle <<"\n";};
        void FancyPrintVector();
        void scale(double a) { length *= a; }   // extend or contract vector
        vector operator +(vector b);
  

};

vector::vector(const vector &obj) {
   std::cout << "Copy constructor called.\n";
   angle = obj.angle;
   length = obj.length;
}

vector vector::operator +(vector b){
    vector c;
    double cx, cy;
    cx = length*std::cos(angle) + b.length*std::cos(b.angle);
    cy = length*std::sin(angle) + b.length*std::sin(b.angle);
    c.length = std::sqrt(cx*cx + cy*cy);
    c.angle = std::acos(cx/c.length);
    return c;
}

void vector::FancyPrintVector(){std::cout << "\n Fancy Printing vector === \n length:" << length << " angle:" << angle <<"\n";};

class orthovector: public  vector{
    protected:
        int dir; // 0 = right, 1=up, 2 = left, 3 = down
    public:
        orthovector(){dir = 0; length=0; angle = 0.0;};
        orthovector(int d, double len){
            const double halfpi = 1.507;
            dir = d ;
            angle = d *  halfpi;
            length = len;
        };
};

int main()
{
    orthovector o1(0,5);
    orthovector o2(1,5);
     
   /* v2.scale(2);
    v.PrintVector();
    v2.FancyPrintVector();
    v3.FancyPrintVector(); */
    o1.FancyPrintVector();
    o2.FancyPrintVector();

    return 0;
}