#include <stdio.h>
#define PI 3.141

int main()
{
  float radius;
  float area; 
  printf("\n  Please enter the radius (cms):");
  scanf("%f", &radius);
  area = PI * radius * radius;
  printf("\n  area of circle with radius %f is %f \n", radius, area);

  return 0;
}


