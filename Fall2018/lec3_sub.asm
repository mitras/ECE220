.orig x3000

MAIN
	LD R1, VAL_X
	LD R2, VAL_X
	JSR MULT		; R0 <- X^2
	ADD R4, R0, #0		; R4 <- X^2
	ADD R1, R0, #0		; R1 <- X^2
	JSR MULT		; R0 <- X^3
	ADD R5, R0, #0		; R5 <- X^3
	LD R1, VAL_A
	ADD R2, R5, #0
	JSR MULT		; R0 <- aX^3
	ADD R5, R0, #0
	LD R1, VAL_B
	ADD R2, R4, #0
	JSR MULT		; R0 <- bx^2
	ADD R2, R0, #0		;
	ADD R1, R5, #0
	JSR SUBTR		; R0 < aX^3 - bx^2
	ADD R5, R0, #0
	LD R1, VAL_C
	LD R2, VAL_X
	JSR MULT
	ADD R6, R5, R0		; R6 <- aX^3 - bx^2 + cx
				;
	HALT

	
VAL_A .FILL x7
VAL_B .FILL x8
VAL_C .FILL x9
VAL_X .FILL x11

; R0 <- R1 - R2
SUBTR
	;;  INPUT R1, R2
	;; OUTPUT R0
	ST R2, saveR2SUBTR
	NOT R2, R2
	ADD R2, R2, #1		;
	ADD R0, R1, R2
	LD R2, saveR2SUBTR			;
	RET

saveR2SUBTR .BLKW 1 	
	


; R0 <- R1 * R2
MULT
	;;  INPUT R1, R2
	;; OUTPUT R0
	ST R1, saveR1MULT
	AND R0, R0, #0		;
MLOOP	ADD R0, R2, R0		;
	ADD R1, R1, #-1		;
	BRp MLOOP
	LD R1, saveR1MULT
	RET

saveR1MULT .BLKW 1 	



.END
