#include <stdio.h>
#include <math.h>
// finding roots of a quadratic equations

int main()
{
  float a,b,c,d,x1,x2;
  scanf("%f %f %f",&a, &b,&c);
  d = b*b - 4 * a  * c;
  if (d == 0)
    {
      x1 = -b / (2 * a);
      x2 = x1;
      printf("\n repeated root at %f", x1);         
    }
  else if ( d > 0)
    {
      x1 = (-b + sqrt(d)) / (2 * a);
      x2 = (-b - sqrt(d)) / (2 * a);
      printf("\n real roots at %f %f", x1, x2);         
    }
  else 
    {
      x1 = sqrt(-d) / (2 * a);
      x2 = -b/ (2 * a);
      printf("\n complex roots at %f+i %f and  %f -i %f", x2, x1, x2, x1);         
    }

  return 0;
  }
