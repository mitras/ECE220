#include <stdio.h>
#define N 10
/* Comutes the average of N integers */


float compAverage(float* input_values);

int main ()
{
	int i;
	float nums[N] = {0.9, 1.8, 4.5, 1.8, 4.5, 1.8, 4.5, 1.8, 4.5, 7};
	/* reads inputs and stores them in array */

	printf("The average = %f \n", compAverage(&nums[0]));
	return 0;

}

/* computes and returns average */
/* array called by reference */
float compAverage(float* input_values)
{
	float average = 0.0;
	int i;
	for (i=0; i<N;i++)
	  average += *(input_values+i); //  average +=input_values[i]
	return average = average/N;	
}


