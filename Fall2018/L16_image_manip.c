#include<stdio.h>

int main()
{
  FILE* input;
  FILE* output_pic;
  int image[1178][786];
  char type[10];
  int width, height, maxp, i,j;
  int header;

  input = fopen("alma.pgm", "r");
   if (input == NULL)
    {
      printf("Could not open file %s\n", "alma.pgm");
      return -1;
    }
  output_pic = fopen("alma-out.pgm", "w");
   if (output_pic == NULL)
    {
      printf("Could not open file %s\n", "alma-out.pgm");
      fclose(input);
      return -1;
    }
    
     header = fscanf(input, "%s %d %d %d", type, &width, &height, &maxp);
     if (header != 4)
       {
	 printf("Error reading image header\n");
	  fclose(input);
	  fclose(output_pic);
	  return -1;
       }
    
     printf("\n%s \n %d \n %d  \n %d \n", type, width, height, maxp);
     fprintf(output_pic, "%s \n %d  %d  \n %d \n", type, width, height, maxp);

     for (i=0;i<width;i++)
       for (j=0;j<height;j++)
	 {
	   fscanf(input,"%d", &image[i][j]);
	   fprintf(output_pic," %d ", 255-image[i][j]);
	 }

     fclose(input);
     fclose(output_pic);
     return 0;

}
