#include <stdio.h>
#include <stdlib.h>

typedef struct tree TreeNode;

struct tree {
    TreeNode* lSubtree;
    TreeNode* rSubtree;
    int data;
};

TreeNode * const LEAF = NULL;

TreeNode * new_tree(int);

void preorder(TreeNode * root);
void inorder(TreeNode * root);
void postorder(TreeNode * root);

TreeNode* bst_search(TreeNode * root, int x);
void bst_search_edge(TreeNode * root, TreeNode ** here, int x);
void bst_insert(TreeNode ** root, int newData);
TreeNode * bst_delete(TreeNode * root, int val);
void bst_find_succ(TreeNode * start, TreeNode ** succ);

TreeNode * build_tree1();
TreeNode * build_tree2();

int main ()
{
    TreeNode* tree1 = build_tree1();
    TreeNode* tree2 = build_tree2();
 
    /* Binary Tree Traversal */

    printf("In-order Tree1: ");
    inorder(tree1);
    printf("\n");
    printf("Pre-order Tree1: ");
    preorder(tree1);
    printf("\n");
    printf("Post-order Tree1: ");
    postorder(tree1);
    printf("\n");

    printf("In-order Tree2: ");
    inorder(tree2);
    printf("\n");
    printf("Pre-order Tree2: ");
    preorder(tree2);
    printf("\n");
    printf("Post-order Tree2: ");
    postorder(tree2);
    printf("\n");

    /* Binary *Search* Tree Operations */

    printf("Search x = 36 and print its subtree: ");
    {
        TreeNode* t = bst_search(tree2, 36);
        inorder(t);
    }
    printf("\n");

    printf("Search edge to node x = 29 and print subtree inorder: ");
    {
        TreeNode* edge = NULL;
        bst_search_edge(tree2, &edge, 29);
        inorder(edge);
    }
    printf("\n");

    printf("Find succ for x = 20: ");
    {
        TreeNode* t = bst_search(tree2, 20);
        TreeNode* succ = LEAF;
        bst_find_succ(t, &succ);
        printf("%d", succ->data);
    }
    printf("\n");

    printf("Delete x = 42 and print inorder: ");
    {
        bst_delete(tree2, 42);
        inorder(tree2);
    }
    printf("\n");

    return 0;
}

void inorder(TreeNode * root)
{
    if (root==LEAF)
        return;
    else
    {
        inorder(root->lSubtree);
        printf(" %d", root->data);
        inorder(root->rSubtree);
    }
}

void preorder(TreeNode * root)
{
    if (root ==LEAF)
        return;
    else
    {
        printf(" %d", root->data);
        preorder(root->lSubtree);
        preorder(root->rSubtree);
    }
}

void postorder(TreeNode * root)
{
    if (root==LEAF)
        return;
    else
    {
        postorder(root->lSubtree);
        postorder(root->rSubtree);
        printf(" %d", root->data);
    }
}

TreeNode * new_tree(int newData) {
    TreeNode * newtree = (TreeNode*) malloc (sizeof(TreeNode));

    if (newtree == NULL)
    {
        printf("\n Failed to create node\n");
        return newtree;
    }
    else {
        newtree->lSubtree = LEAF;
        newtree->rSubtree = LEAF;
        newtree->data = newData;
        return newtree;
    }
}

TreeNode * bst_search(TreeNode * root, int x)
{
    if (root == LEAF)
        return LEAF;
    else if (x == root->data)
        return root;
    else if (x < root->data)
        return bst_search(root->lSubtree,x);
    else if (x > root->data)
        return bst_search(root->rSubtree,x);
}

void bst_search_edge(TreeNode * root, TreeNode ** here, int x)
{
    if (root == LEAF)
        *here = NULL;
    else if (x == root->data)
        *here = root;
    else if (x < root->data) {
        bst_search_edge(root->lSubtree, here, x);
    }
    else if (x > root->data) {
        bst_search_edge(root->rSubtree, here, x);
    }
}

void bst_insert(TreeNode ** root, int newData)
{
    if (*root == LEAF)
    {
        *root = new_tree(newData);
        return;
    }
    else
    {
        if ((*root)->data > newData)
            bst_insert(&(*root)->lSubtree, newData);
        else if ((*root)->data < newData)
            bst_insert(&(*root)->rSubtree, newData);
        else
            /* ?? */;
    }
}

TreeNode * bst_delete(TreeNode * root, int val)
{
    // base case 
    if (root == LEAF) return root; 
  
    // Find the node, and update the edge because this node can be the parent.
    if (val < root->data) 
        root->lSubtree = bst_delete(root->lSubtree, val);
    else if (val > root->data) 
        root->rSubtree = bst_delete(root->rSubtree, val);
    // if val is same as root's data, then This is the node to be deleted
    else
    { 
        // node with no child 
        if (root->lSubtree == LEAF && root->rSubtree == LEAF)
        {
            free(root);
            return LEAF;
        }
        // node with only one child
        else if (root->lSubtree == LEAF)
        { 
            TreeNode * temp = root->rSubtree;
            free(root); 
            return temp; 
        } 
        else if (root->rSubtree == LEAF) 
        { 
            TreeNode * temp = root->lSubtree;
            free(root); 
            return temp; 
        }

        // node with two children: Get the inorder successor
        TreeNode * succ;
        bst_find_succ(root, &succ);
  
        // Copy the inorder successor's content to this node 
        root->data = succ->data;
  
        // Delete the inorder successor 
        root->rSubtree = bst_delete(root->rSubtree, succ->data);
    } 
    return root; 
}

void bst_find_succ(TreeNode * start, TreeNode ** succ)
{
    // Find smallest in the right subtree
    TreeNode * node = start->rSubtree;
    while(node->lSubtree != LEAF)
        node = node->lSubtree;
    *succ = node;
}

TreeNode * build_tree1()
{
    /* Bad way to construct tree. For live demo purpose only */
    TreeNode* t0 = new_tree(0);
    TreeNode* t1 = new_tree(1);
    TreeNode* t2 = new_tree(2);
    TreeNode* t3 = new_tree(3);
    TreeNode* t4 = new_tree(4);
    TreeNode* t5 = new_tree(5);
    TreeNode* t6 = new_tree(6);

    t0->lSubtree = t1;
    t0->rSubtree = t2;

    t1->lSubtree = t3;
    t1->rSubtree = t4;

    t2->lSubtree = t5;
    t2->rSubtree = t6;

    return t0;
}

TreeNode * build_tree2()
{
    /* Bad way to construct tree. For live demo purpose only */
    TreeNode* t42 = new_tree(42);
    TreeNode* t20 = new_tree(20);
    TreeNode* t78 = new_tree(78);
    TreeNode* t12 = new_tree(12);
    TreeNode* t36 = new_tree(36);
    TreeNode* t56 = new_tree(56);
    TreeNode* t98 = new_tree(98);
    TreeNode* t29 = new_tree(29);
    TreeNode* t112= new_tree(112);

    t42->lSubtree = t20;
    t42->rSubtree = t78;

    t20->lSubtree = t12;
    t20->rSubtree = t36;

    t78->lSubtree = t56;
    t78->rSubtree = t98;

    t36->lSubtree = t29;

    t98->rSubtree = t112;

    return t42;
}