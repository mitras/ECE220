#include<iostream>
#include <cmath>

//using namespace std;

class vector{
    protected:
        double length, angle;


    public:
        // constructors
        vector() {std::cout << "\n Default constructor called\n";}
        vector(double l, double a) {std::cout << "\n Second constructor called\n"; length = l; angle = a;}
        vector(const vector &vec); // copy constructor
        // destructor
        //~vector() {std::cout<< "\n Destructor called \n";}  
        // member methods
        double getlenght(){return length;};
        void PrintVector();
        // operations on vectors
        void scale(double a) {length *= a;} ; 
        vector operator +(vector b);
        /*{
            vector c;
            double cx, cy;
            cx = std::cos(angle)*length + std::cos(b.angle)* b.length;
            cy = std::sin(angle)*length + std::sin(b.angle)* b.length;
            c.length = std::sqrt(cx*cx + cy*cy);
            c.angle = std::acos(cx/c.length);
            return c;
        };*/

        friend double flip(vector v);
        friend class LinkedList;


};


void vector::PrintVector(){
    std::cout << "\n length = " << length << " angle = " << angle << std::endl;
};

vector::vector(const vector &vec){
    std::cout << "\n Copy constructor called\n";
    angle = vec.angle;
    length = vec.length;
};

double flip(vector v){
    v.angle = -1*v.angle;
    std::cout << "\n accessing angle:" << v.angle << std::endl;
    return v.angle;
};

vector  vector::operator +(vector b){
    vector c;
    double cx, cy;
    cx = std::cos(angle)*length + std::cos(b.angle)* b.length;
    cy = std::sin(angle)*length + std::sin(b.angle)* b.length;
    c.length = std::sqrt(cx*cx + cy*cy);
    c.angle = std::acos(cx/c.length);
    return c;
};

class LinkedList{

    public:
    void foo(vector v){std::cout<< v.angle;};
};

// orthovector class derived from vector class 
class orthovector: public vector{
    protected:
        int direction; // 0 = right, 1 = up, 2 = left, 3 = left
    public:
        orthovector(){direction=0; length = 0.0; angle = 0.0;};
        orthovector(int dir, double len)
            {
                const double halfPi = 1.50796;
                direction = dir;
                length = len;
                angle = dir*halfPi;
            };
        void foo(){PrintVector();}

};

int main()
{
   /* vector v1(1,0.5); 
    vector v2(1,-0.5); 
    v2.scale(2);
    v2.PrintVector();
    flip(v2);
    v2.PrintVector();
    vector v3 = v2;
    v3 = v3 + v2; */
    orthovector o1(0,5);
    orthovector o2(1,5);
    o1.PrintVector();
    o2.PrintVector();
    vector v3;
    v3 = (vector)o2 + o1;
    v3.PrintVector();
    return 0;
}