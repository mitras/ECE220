.ORIG x3000

;;; Demonstration of a stack

MAIN
	;; Pushes multiples of in the stack and then pops them
	AND R4,R4,#0		;
	AND R1, R1,#0
	LD R2, ASCII
	ADD R1, R1, #5
PUSHLOOP ADD R4, R4, #1
	ADD R0, R4, #0
	JSR PUSH 
	ADD R1, R1, #-1
	BRP PUSHLOOP
	ADD R1, R1, #6
POPLOOP 
	JSR POP
	ADD R0, R0, R2
	OUT
	ADD R1, R1, #-1
	BRP POPLOOP
	HALT

ASCII .fill  x30 
	
	





;;; Pushes R0 at the top of the stack
;;; IN: R0
;;; OUT: R5 (0-success; 1 fail)
;;; USES: R3: stack end, R4: stack_top




	
PUSH
	;; Save registers
	ST R3, saveR3push
	ST R4, saveR4push
	;; Set R5 to 0
	AND R5, R5, #0
	;; Lod top and end
	LD R3, stack_end
	LD R4, stack_top
	;; check full: end-1 = top
	ADD R3, R3, #-1
	NOT R3, R3
	ADD R3, R3, #1
	Add R3, R4, R3		;R3 = top - (end -1)
	BRz OVERFLOW
	;; not overflow
	STR R0, R4, #0		;push R0
	ADD R4, R4, #-1
	ST R4, stack_top	;move top of stack up by one
	BRNZP PUSH_DONE
OVERFLOW
	ADD R5, R5, #1
PUSH_DONE
	;; restore registers
	LD R3, saveR3push
	LD R4, saveR4push
	RET
	
	saveR4push .BLKW 1
	saveR3push .BLKW 1
	
;;; POPs and stores in R0
;;; IN: none
;;; OUT: R5 (0-success; 1 fail); poped value in R0
;;; USES: R3: stack_start, R4: stack_top
	
POP
	;; Save registers
	ST R3, saveR3pop
	ST R4, saveR4pop
	;; Set R5 to 0
	AND R5, R5, #0
	;; Lod top and end
	LD R3, stack_start
	LD R4, stack_top
	;; check full: start = top
	NOT R3, R3
	ADD R3, R3, #1
	Add R3, R4, R3		;R3 = top - start
	BRz UNDERFLOW
	;; not underflow
	LDR R0, R4, #0		;R0<- popped value
	ADD R4, R4, #1		;update stack pointer
	ST R4, stack_top
	BRNZP POP_DONE
UNDERFLOW
	ADD R5, R5, #1
POP_DONE
	;; restore registers
	LD R3, saveR3pop
	LD R4, saveR4pop
	RET
	
	saveR4pop .BLKW 1
	saveR3pop .BLKW 1

	stack_start .FILL x3600
	stack_end .FILL x35F0
	stack_top .FILL x3600
	.END
