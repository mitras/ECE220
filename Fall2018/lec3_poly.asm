.ORIG x3000

	
;;; MAIN
	LD R1, VAL_X
	LD R2, VAL_X
	JSR MULT		;R0<- X^2
	ADD R4, R0, #0		;R4 <- X^2
	ADD R1, R4, #0		;R1 <- X2
	JSR MULT		;R0 <- X3
	ADD R5, R0, #1 		;
	LD R2, VAL_A
	ADD R1, R5, #0		;
	JSR MULT		; R0 <- a X^#
	



VAL_A .FILL x7
VAL_B .FILL x5
VAL_C .FILL x9
VAL_X .FILL x4
	

SUBTR
;;; INPUT: R1, R2,
;;; OUTPUT: R0 = R1 - R2

	ST R2, saveR2SUB
	ST R4, saveR4SUB
	NOT R2, R2
	ADD R2, R2, #1
	ADD R0, R1, R2
	LD R2, saveR2SUB
	LD R4, saveR4SUB
	RET

	saveR2SUB .BLKW 1

MULT
;;; INPUT: R1, R2
;;; OUTPUT: R0 <- R1 * R2
;;; Assumes all positive
;;; preserves the values of R1, R2


.END
