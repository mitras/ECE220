#include<stdio.h>

int binarySearch(int array[], int start, int end, int index);

int main()
{
  int array[10] = {1,20,32,48,59,64, 72, 80, 91, 190};
  int x = 32;
  printf("\n Element %d found at position %d", x, binarySearch(array,0,9,x));
  return 0;
}


int binarySearch(int array[], int start, int end, int x)
// finds item in array of size Size
// returns index < Size if found
// else returns -1
{
  int mid;
  int mid_item;
  if (end < start)
    return -1;
  else 
    {
      mid = (start + end)/2;
      mid_item = array[mid];
      if (mid_item == x)
	return mid;
      if (mid_item > x)
	return binarySearch(array, start, mid-1, x);
      if (mid_item < x)
	return binarySearch(array, mid+1, end, x);

    }
  
}
