.ORIG x3000
	
	;; Read 10 chars from kbd and print to screen
	AND R4, R4, #0		;
	ADD R4, R4, #10		;
RLOOP   LDI R1, KBSRadd
	BRZP RLOOP
	LDI R0, KBDRadd		;read keyboard in R0
WLOOP   LDI R1, DSRadd
	BRZP WLOOP
	STI R0, DDRadd
	ADD R4, R4, #-1
	BRP RLOOP
	HALT



KBSRadd .FILL xFE00
KBDRadd .FILL xFE02
DSRadd .FILL xFE04
DDRadd .FILL xFE06

.END
	
