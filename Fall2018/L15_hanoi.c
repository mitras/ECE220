#include <stdio.h>

void movetower(int size, int froma, int tob);

int main()
{
  int n;
  scanf("%d", &n);
  printf("Move tower %d from 0 to 2\n", n);
  movetower(n,0,2);
  return(0);
}

void movetower(int n, int a, int b)
// a,b = {0,1,2}, a != b.
// Note. if a = 0, b = 1, then 3 - a - b = 2 
// Note. if a = 2, b = 1, then 3 - a - b = 0  
// Note. if a = 2, b = 0, then 3 - a - b = 1  
// 3 - a - b != a, !=b
{
  if (n==1)
    {
      printf("\n Moving disc of size %d from pole %d to pole %d", n, a, b);
      return;
    }
  else
    {
      movetower(n-1, a, 3 - a -b);
      printf("\n Moving disc of size %d from pole %d to pole %d", n, a, b);
      movetower(n-1, 3 - a - b, b);
    }

}



