#include <stdio.h>

int swap(int * arg_a, int * arg_b);

int main()
{
  int a = 4;
  int b = 400;
  printf("\n Before a = %d, b = %d", a,b);
  printf("\n addresses a = %p, b = %p", &a,&b);
  swap(&a,&b);
  printf("\n After a = %d, b = %d", a,b);
  printf("\n addresses a = %p, b = %p", &a,&b);
  
  return 0;
}


int swap(int * arg_a, int * arg_b)
{
  int temp;
  temp = *arg_a;
  *arg_a = *arg_b;
  *arg_b = temp;
  return 0;
}
