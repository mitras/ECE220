#include<stdio.h>
#define n 3
#define m 3
#define p 3

void printmatrix(float* A, int nn, int mm);
void initmatrix(float A[n][n], int nn, int mm);
void multiply(float A[n][m], float B[m][p],float C[n][p], int nn, int mm, int pp);


int main()
{
	float A[n][m];
	float B[m][p];
	float C[n][p];

	int i,j,k;
	initmatrix(A,n,m);
	initmatrix(B,m,p);
	initmatrix(C,n,p);
	
	for (i=0;i<n;i++)
	    A[i][i] = 5;
	A[1][0]=4.5;
	A[1][2]=3.3;
	for (i=0;i<n;i++)
	    B[2][i] = 2;
	for (i=0;i<n;i++)
	    B[1][i] = 3;

	printf("\n A = ");
	printmatrix((float *)A, n, m);
	printf("\n B = ");
	printmatrix((float *)B, m, p);
	printf("\n Before C = ");
	printmatrix((float *)C, n, p);
	printf("\n After C = ");
	multiply(A,B,C, n, m,p);
	printmatrix((float *)C, n, p);

	return 0;
}

void initmatrix(float A[n][m], int nn, int mm)
{
  printf("\n Initializing matrix \n");
  int i,j;
  for(i=0;i<nn;i++)
    {
    for(j=0;j<mm;j++)
      A[i][j] = 0;
     }
  return;
}

void printmatrix(float* A, int nn, int mm)
{
  printf("\n Printing matrix \n");
  int i,j;
  for(i=0;i<nn;i++)
    {
    for(j=0;j<mm;j++)
      printf(" %0.3f ", *((A + nn*i) + j));
    printf("\n");
    }
  return;
}

void multiply(float A[n][m], float B[m][p],float C[n][p], int nn, int mm, int pp)
{
  int i,j,k;
  float S=0;
  for(i=0;i<nn;i++)
    for(j=0;j<pp;j++)
      {
	S = 0;
	for (k=0;k<mm;k++)
	  S += A[i][k]*B[j][k];
	C[i][j] = S;    
      }    
  return;
}

